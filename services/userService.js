const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    createUser(data) {
        UserRepository.create(data);
    }

    getUsers(){
        return UserRepository.getAll();
    }

    getUser(id){
        return UserRepository.getOne({ "id": id });
    }

    updateUser(id, data) {
        UserRepository.update(id, data);
    }

    deleteUser(id) {
        UserRepository.delete(id);
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new UserService();