const { FighterRepository } = require('../repositories/fighterRepository');
const { search } = require('./userService');

class FighterService {
    // TODO: Implement methods to work with fighters
    createFighter(data) {
        FighterRepository.create(data);
    }

    getFighters(){
        return FighterRepository.getAll();
    }

    getFighter(id){
        return FighterRepository.getOne({ "id": id });
    }

    updateFighter(id, data) {
        FighterRepository.update(id, data);
        return this.getFighter(id);
    }

    deleteFighter(id) {
        FighterRepository.delete(id);
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
}

module.exports = new FighterService();