const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.get('/', (req, res, next) => {
    const users = UserService.getUsers();
    res.status(200).json(users);
});

router.get('/:id', (req, res, next) => {
    const user = UserService.getUser(req.params.id);
    res.status(200).json(user);
});

router.post('/', createUserValid, (req, res, next) => {
    const user = UserService.createUser(req.body);
    res.status(200).json({ 
        created: true 
    });
});

router.put('/:id', updateUserValid, (req, res, next) => {
    const userDataToUpdate = req.body;
    UserService.updateUser(req.params.id, userDataToUpdate);
    res.status(200).json({ 
        updated: true 
    });
});

router.delete('/:id', (req, res, next) => {
    UserService.deleteUser(req.params.id);
    res.status(400).json({ deleted: true });
});

module.exports = router;