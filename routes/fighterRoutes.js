const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.get('/', async (req, res, next) => {
    const fighters = await FighterService.getFighters();
    if(fighters == '' || typeof(fighters) == 'undefined'){
        res.status(404).json({ "error": true, "message": "Fighters not found"});
    }
    res.status(200).json(fighters);
});

router.get('/:id', async (req, res, next) => {
    const fighter = await FighterService.getFighter(req.params.id); console.log(fighter);
    if(fighter == '' || typeof(fighter) == "undefined"){
        res.status(404).json({ "error": true, "message": `Fighter with id ${req.params.id} not found`});
    }
    res.status(200).json(fighter);
});

router.post('/', createFighterValid, (req, res, next) => {
    FighterService.createFighter(req.body);
    res.status(200).json({ created: true,  });
});

router.put('/:id', updateFighterValid, async (req, res, next) => {
    const fighterDataToUpdate = req.body;
    const updatedFighterData = FighterService.updateFighter(req.params.id, fighterDataToUpdate);
    res.status(200).json({ updated: true });
});

router.delete('/:id', (req, res, next) => {
    FighterService.deleteFighter(req.params.id);
    res.status(404).json({ deleted: true });
});


module.exports = router;