const { user } = require('../models/user');
const userService = require('../services/userService');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    let requestData = req.body;
    
    if(Object.keys(requestData).length !== 0 && requestData.constructor === Object) {
        let notAvailableField = Object.keys(requestData).find( key => checkNotAvailableField(key) );
        if(notAvailableField){
            return res.status(400).json({ "error": true, "message" : `Parameter ${notAvailableField} is denied in body request` });
        }
        
        if(isFieldEmpty( requestData.firstName )) {
            res.status(400).json({ "error": true, "message": "Please enter user firstName" });
        }
        if( isFieldEmpty( requestData.lastName ) ) {
            return res.status(400).json({ "error": true, "message": "Please enter user secondName" });
        }
        if( isFieldEmpty( requestData.email ) || !isValidEmail(requestData.email) ) {
            return res.status(400).json({ "error": true, "message": "Please enter correct user Google email (<your.email>@gmail.com)" });
        }
        if( isFieldEmpty( requestData.phoneNumber ) || !isValidPhoneNumber( requestData.phoneNumber ) ) {
            return res.status(400).json({ "error": true, "message": "Please enter correct user phoneNumber (+380*********)" });
        }
        if( isFieldEmpty( requestData.password ) || !isValidPassword( requestData.password ) ) {
            return res.status(400).json({ "error": true, "message": "Please enter correct user password (at least 3 characters)" });
        }
        if(!isEmailUnique( requestData.email )){
            return res.status(400).json({ "error": true, "message": "Email already exists" });
        } 
        if(!isPhoneUnique( requestData.phoneNumber )) {
            return res.status(400).json({ "error": true, "message": "Phone already exists" });
        } 
    }
    else {
        return res.status(400).json({ "error": true, "message" : "Body request is empty" });
    }

    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    let requestData = req.body;
    if(Object.keys(requestData).length !== 0 && requestData.constructor === Object) {
        let user = isUserFound(req.params.id);
        if(!user){
            return res.status(404).json({ "error": true, "message" : `User with id ${req.params.id} not found` });
        }

        let notAvailableField = Object.keys(requestData).find( key => checkNotAvailableField(key) );
        if(notAvailableField){
            return res.status(400).json({ "error": true, "message" : `Parameter ${notAvailableField} is denied in body request` });
        }
        
        if(isFieldEmpty( requestData.firstName )) {
            return res.status(400).json({ "error": true, "message": "Please enter user firstName" });
        }
        if( isFieldEmpty( requestData.lastName ) ) {
            return res.status(400).json({ "error": true, "message": "Please enter user secondName" });
        }
        if( isFieldEmpty( requestData.email ) || !isValidEmail(requestData.email) ) {
            return res.status(400).json({ "error": true, "message": "Please enter correct user Google email (<your.email>@gmail.com)" });
        }
        if( isFieldEmpty( requestData.phoneNumber ) || !isValidPhoneNumber( requestData.phoneNumber ) ) {
            return res.status(400).json({ "error": true, "message": "Please enter correct user phoneNumber (+380*********)" });
        }
        if( isFieldEmpty( requestData.password ) || !isValidPassword( requestData.password ) ) {
            return res.status(400).json({ "error": true, "message": "Please enter correct user password (at least 3 characters)" });
        }
        if(!isEmailUnique( requestData.email )){
            return res.status(400).json({ "error": true, "message": "Email already exists" });
        } 
        if(!isPhoneUnique( requestData.phoneNumber )) {
            return res.status(400).json({ "error": true, "message": "Phone already exists" });
        }     
    }
    else {
        return res.status(400).json({ "error": true, "message" : "Body request is empty" });
    }

    next();
}

const isEmailUnique = ( email ) => {
    let userEmail = userService.search( { "email" : email} );
    if(userEmail) {
        return false;
    }

    return true;
}

const isPhoneUnique = ( phone ) => {
    let userPhoneNumber = userService.search( { "phoneNumber" : phone} );
    if(userPhoneNumber) {
        return false;
    }

    return true;
}

const isUserFound = ( id ) => {
    let user = userService.search( { "id" : id} );
    if(user) {
        return user;
    }

    return false;
}

const checkNotAvailableField = ( fieldName ) => {
    if(fieldName == 'id') {
        return true;
    }

    return !user.hasOwnProperty(fieldName);
} 

const isFieldEmpty = ( field ) => {
    if(typeof(field) === 'undefined' || field === ''){
        return true;
    }
    return false;
}

const isValidPhoneNumber = (phoneNumber) => {
    const phoneNumberTemplate = /^\+380\d{9}$/;
    return phoneNumberTemplate.test(phoneNumber);
}

const isValidEmail = ( email ) => {
    const emailTemplate = /^[\w-\.]+@gmail.com$/;
    return emailTemplate.test(String(email).toLowerCase());
}

const isValidPassword = ( password ) => {
    if(password.length < 3){
        return false;
    }

    return true;
}
exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;