const { request } = require('chai');
const { fighter } = require('../models/fighter');
const fighterService = require('../services/fighterService');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    let requestData = req.body;
    
    if(Object.keys(requestData).length !== 0 && requestData.constructor === Object) {
        let notAvailableField = Object.keys(requestData).find( key => checkNotAvailableField(key) );
        if(notAvailableField){
            return res.status(400).json({ "error": true, "message" : `Parameter ${notAvailableField} is denied in body request` });
        }
        if(isFieldEmpty( requestData.name )) {
            return res.status(400).json({ "error": true, "message": "Please enter fighter name" });
        }
        if( isFieldEmpty( requestData.health )){
            requestData.health = 100;
        }
        if( isFieldEmpty( requestData.power ) || typeof(requestData.power) !== "number" || (requestData.power <= 0 || requestData.power > 100)) {
            return res.status(400).json({ "error": true, "message": "Fighter power should be a number between values 0 and 100" });
        }
        if( isFieldEmpty( requestData.defense ) || typeof(requestData.defense) !== "number" || (requestData.defense < 1 || requestData.defense > 10)) {
            return res.status(400).json({ "error": true, "message": "Fighter defense should be a number between values 1 and 10" });
        }
        if(isFighterNameExists( requestData.name )){
            return res.status(400).json({ "error": true, "message": "Fighter with the same name already exists" });
        }
    }

    else {
        return res.status(400).json({ "error": true, "message" : "Body request is empty" });
    }

    next();
}

const checkNotAvailableField = ( fieldName ) => {
    if(fieldName == 'id') {
        return true;
    }

    return !fighter.hasOwnProperty(fieldName);
} 

const isFieldEmpty = ( field ) => {
    if(typeof(field) === 'undefined' || field === ''){
        return true;
    }
    return false;
}

const isFighterNameExists = ( name ) => {
    let fighter = fighterService.search( { "name" : name } );
    if(fighter) {
        return true;
    }

    return false;
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    let requestData = req.body;
    if(Object.keys(requestData).length !== 0 && requestData.constructor === Object) {
        let notAvailableField = Object.keys(requestData).find( key => checkNotAvailableField(key) );
        if(notAvailableField){
            return res.status(400).json({ "error": true, "message" : `Parameter ${notAvailableField} is denied in body request` });
        }
        if(isFieldEmpty( requestData.name )) {
            return res.status(400).json({ "error": true, "message": "Please enter fighter name" });
        }
        if( isFieldEmpty( requestData.health ) || typeof(requestData.health) !== "number" || (requestData.health <= 0)) {
            return res.status(400).json({ "error": true, "message": "Fighter health should be a number and not equal or less then 0" });
        }
        if( isFieldEmpty( requestData.power ) || typeof(requestData.power) !== "number" || (requestData.power <= 0 || requestData.power > 100)) {
            return res.status(400).json({ "error": true, "message": "Fighter power should be a number between values 0 and 100" });
        }
        if( isFieldEmpty( requestData.defense ) || typeof(requestData.defense) !== "number" || (requestData.defense < 1 || requestData.defense > 10)) {
            return res.status(400).json({ "error": true, "message": "Fighter defense should be a number between values 1 and 10" });
        }
        if(isFighterNameExists( requestData.name )){
            return res.status(400).json({ "error": true, "message": "Fighter with the same name already exists" });
        }
    }
    else {
        return res.status(400).json({ "error": true, "message" : "Body request is empty" });
    }

    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;