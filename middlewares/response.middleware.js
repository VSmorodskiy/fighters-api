const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if(res.data){
        res.sendStatus(200);
    }
    else {
        res.sendStatus(400).json({"error": true, "message": "Incorrect email or password"});
    }
    
    next();
}

exports.responseMiddleware = responseMiddleware;